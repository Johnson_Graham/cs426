﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cs426
{
    public abstract class Definition
    {
        public string name;
    }
    public abstract class TypeDefinition : Definition
    {
        public Definition definition;
        public string typename;
        public bool isconst;
    }
    public class BasicType : TypeDefinition
    {
        
    }
    public class ArrayType : TypeDefinition
    {
       // public TypeDefinition arraytype;
        public int arraylength;
        public string[] contents;
    }
    public class VariableDefinition : TypeDefinition
    {
        public TypeDefinition vartype;
        //public string vartype;
    }
    public abstract class SubProgramDefinition : Definition
    {
        public string rtype;
        public System.Collections.Generic.Dictionary<string, Definition>
            contents = new Dictionary<string, Definition>();
    }
    public class FunctionDefinition : SubProgramDefinition
    {
        //public SubProgramDefinition functionprog;

    }
}

