Testing basic variable declaration:
Created variables int x, int y, int z, and float r for later use.
Testing basic assignment and math:
x = 2 + 3. x should equal 5: 
5
y = 5 - 4. y should be equal to 1:
1
z = 3 * 5. z should be equal to 15:
15
Float assignment. r = 8.3 / 1.0. r should equal 8.3
8.3
Let assignment demonstration:
Assigning final float m to 5.2/3.1+1.4*9.3.
14.69742
Assigning final int fin to 2.
2
Assigning final float fn to 2.1.
2.1
Testing array declaration and assignment and loading from arrays:
Creating an array of length 3 called iarr
Assigning iarr[1] to 2 and printing iarr[1]
2
Assigning z to iarr[1]. z should be 2
2
Creating an array farr of length 4
Assigning farr[0] to 2.317. Prining farr[0] should print 2.317
2.317
Assigning r to farr[0]. r should be 2.317
2.317
Testing the test function:
in hey()
This is back to my main function
Testing in another function
2
2.317
In the function
This is back to my main function
Testing the while loop. x<5 && 2>1 Should loop 4 times:
In the while loop
In the while loop
In the while loop
In the while loop
While loop, demonstrate not. x<= 6 && 1<2 Should loop twice:
In the while loop
In the while loop
