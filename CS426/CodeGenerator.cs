﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cs426.node;

namespace cs426
{
    class CodeGenerator : cs426.analysis.DepthFirstAdapter
    {
        System.Collections.Generic.Dictionary<string, Definition>
        typehash = new Dictionary<string, Definition>();
        System.Collections.Generic.Dictionary<node.Node, Definition>
        nodehash = new Dictionary<node.Node, Definition>();
        int loopCounter = 1;
        int argumentCounter = 0;

        // PRODUCTION 1 -------------------------------------------------------------
        // Program
        public override void InAMainfuncProgram(AMainfuncProgram node)
        {
            BasicType inttype = new BasicType();
            inttype.name = "int";
            BasicType floattype = new BasicType();
            floattype.name = "float";
            typehash.Add(inttype.name, inttype);
            typehash.Add(floattype.name, floattype);

            Console.WriteLine(".assembly extern mscorlib {}");
            Console.WriteLine(".assembly Test");
            Console.WriteLine("{");
            Console.WriteLine(".ver 1:0:0:0");
            Console.WriteLine("}");
            Console.WriteLine(".class Program {");
            Console.WriteLine(".method static void main() cil managed");
            Console.WriteLine("{");
            Console.WriteLine(".entrypoint");
            Console.WriteLine(".maxstack 128");
            
        }

        public override void OutAMainfuncProgram(AMainfuncProgram node)
        {
            Console.WriteLine("ret");
            Console.WriteLine("}");
            Console.WriteLine("}");
        }

        public override void InAFuncProgram(AFuncProgram node)
        {
            BasicType inttype = new BasicType();
            inttype.name = "int";
            BasicType floattype = new BasicType();
            floattype.name = "float";
            typehash.Add(inttype.name, inttype);
            typehash.Add(floattype.name, floattype);

            Console.WriteLine(".assembly extern mscorlib {}");
            Console.WriteLine(".assembly Test");
            Console.WriteLine("{");
            Console.WriteLine(".ver 1:0:0:0");
            Console.WriteLine("}");
            Console.WriteLine(".class Program");
            Console.WriteLine("{");
        }

        public override void OutAFuncProgram(AFuncProgram node)
        {
            Console.WriteLine("ret");
            Console.WriteLine("}");
            Console.WriteLine("}");
        }

        // PRODUCTION 2 ----------------------------------------------------------------
        // Func list
        public override void OutAManyFunclist(AManyFunclist node)
        {
            Console.WriteLine("ret");
            Console.WriteLine("}");
        }

        public override void InAOneFunclist(AOneFunclist node)
        {
            //Console.WriteLine("{");
        }

        public override void OutAOneFunclist(AOneFunclist node)
        {
            Console.WriteLine("ret");
            Console.WriteLine("}");
            
        }

        //PRODUCTION 3 -----------------------------------------------------------------
        //PROGRAM INTERNAL
        public override void InAManyPrograminternal(AManyPrograminternal node)
        {
            base.InAManyPrograminternal(node);
        }

        public override void InAOnePrograminternal(AOnePrograminternal node)
        {
            base.InAOnePrograminternal(node);
        }

        // PRODUCTION 4 ---------------------------------------------------------------
        // Program Term

        public override void InAVardeclProgramterm(AVardeclProgramterm node)
        {
            base.InAVardeclProgramterm(node);
        }

        public override void InAIfprodProgramterm(AIfprodProgramterm node)
        {
            base.InAIfprodProgramterm(node);
        }

        public override void OutAIfprodProgramterm(AIfprodProgramterm node)
        {
            //Console.WriteLine("br L" + (loopCounter-1).ToString());
        }

        public override void InAWhileprodProgramterm(AWhileprodProgramterm node)
        {
            Console.WriteLine("L" + loopCounter.ToString() + ":");
            loopCounter++;
        }

        public override void OutAWhileprodProgramterm(AWhileprodProgramterm node)
        {
            //Console.WriteLine("brfalse L" + (loopCounter + 1).ToString());
        }

        public override void InAAssignstmtProgramterm(AAssignstmtProgramterm node)
        {
            base.InAAssignstmtProgramterm(node);
        }

        public override void InAProccallProgramterm(AProccallProgramterm node)
        {
            base.InAProccallProgramterm(node);
        }

        // PRODUCTION 5 --------------------------------------------------------------
        // main func
        public override void InAMainfuncMainfunc(AMainfuncMainfunc node)
        {
            
            Console.WriteLine(".method static void main() cil managed");
            Console.WriteLine("{");
            Console.WriteLine(".entrypoint");
            Console.WriteLine(".maxstack 128");
        }

        public override void InANoneMainfunc(ANoneMainfunc node)
        {
            base.InANoneMainfunc(node);
        }

        // PRODUCTION 6 ---------------------------------------------------------------
        // var decl
        public override void InAArrayVardecl(AArrayVardecl node)
        {
            String type = node.GetType().Text;
            String name = node.GetName().Text;
            if (type == "float")
            {
                Console.WriteLine(".locals init(float32[] " + name + ")");
            }
            else if (type == "int")
            {
                Console.WriteLine(".locals init(int32[] " + name + ")");
            }
        }

        public override void OutAArrayVardecl(AArrayVardecl node)
        {
            // TODO : Allocate the array
            String type = node.GetType().Text;
            String name = node.GetName().Text;

            // nodehash.TryGetValue(node.GetExpr(), out elements);
            if (type == "float")
            {
               // Console.WriteLine(".locals init(float32[] " + name + ")");
                //Console.WriteLine("ldc.i4 " + node.GetExpr());
                Console.WriteLine("newarr [mscorlib]System.Single");
                Console.WriteLine("stloc " + name);
            }
            else if (type == "int")
            {
                //Console.WriteLine(".locals init(int32[] " + name + ")");
               // Console.WriteLine("ldc.i4 " + node.GetExpr());
                Console.WriteLine("newarr [mscorlib]System.Int32");
                Console.WriteLine("stloc " + name);
            }
        } 

        public override void OutAIntfloatVardecl(AIntfloatVardecl node)
        {
            String type = node.GetType().Text.Trim();
            String name = node.GetName().Text.Trim();
            //Console.WriteLine(type);
            if (type == "float")
            {
                Console.WriteLine(".locals init(float32 " + name + ")");
            }
            else if (type == "int")
            {
                Console.WriteLine(".locals init(int32 " + name + ")");
            }
        }

        // PRODUCTION 7 --------------------------------------------------------------
        // if prod
        public override void InANoelseIfprod(ANoelseIfprod node)
        {
            Console.WriteLine("br L" + (loopCounter + 1).ToString());
            /*Console.WriteLine("brtrue L" + loopCounter.ToString());
            Console.WriteLine("L" + loopCounter.ToString() + ":");
            loopCounter++;*/
        }

        public override void OutANoelseIfprod(ANoelseIfprod node)
        {
            Console.WriteLine("L" + loopCounter.ToString() + ":");
            loopCounter++;
        }

        public override void InAElseIfprod(AElseIfprod node)
        {
            Console.WriteLine("br L" + (loopCounter).ToString());
        }

        public override void OutAElseIfprod(AElseIfprod node)
        {
            //Console.WriteLine("br L" + (loopCounter).ToString());
            Console.WriteLine("L" + loopCounter.ToString() + ":");
            loopCounter++;
        }

        public override void InAEmptyIfprod(AEmptyIfprod node)
        {
            base.InAEmptyIfprod(node);
        }

        // PROUCTION 8 ---------------------------------------------------------------
        // while prod
        public override void InAWhileWhileprod(AWhileWhileprod node)
        {
            //Console.WriteLine("L" + loopCounter.ToString() + ":");
            //loopCounter++;
        }

        public override void OutAWhileWhileprod(AWhileWhileprod node)
        {
            Console.WriteLine("br L" + (loopCounter - 1).ToString());
            Console.WriteLine("L" + loopCounter.ToString() + ":");
            loopCounter++;
        }

        public override void InAEmptyWhileprod(AEmptyWhileprod node)
        {
            base.InAEmptyWhileprod(node);
        }

        // PRODUCTION 8 --------------------------------------------------------------
        // assign stmt
        public override void InAArrayAssignstmt(AArrayAssignstmt node)
        {
            //base.InAArrayAssignstmt(node);
            string id = node.GetId().Text.Trim();
            if (id != "int" && id != "float")
            {
                Console.WriteLine("ldloc " + id);
            }
        }

        public override void OutAArrayAssignstmt(AArrayAssignstmt node)
        {
            string name = node.GetId().Text;

            //ldloc
            if (node.GetId().Text.StartsWith("i"))
            {
                Console.WriteLine("stelem.i4");
            }
            else if (node.GetId().Text.StartsWith("f"))
            {
                Console.WriteLine("stelem.r4");
            }

        }

        public override void InAConstvarAssignstmt(AConstvarAssignstmt node)
        {
            String type = node.GetType().Text.Trim();
            String name = node.GetName().Text.Trim();
            if (type == "float")
            {
                Console.WriteLine(".locals init(float32 " + name + ")");
            }
            else if (type == "int")
            {
                Console.WriteLine(".locals init(int32 " + name + ")");
            }
        }

        public override void OutAConstvarAssignstmt(AConstvarAssignstmt node)
        {
            String name = node.GetName().Text;
            Console.WriteLine("stloc " + name);
        }

        public override void InAVarAssignstmt(AVarAssignstmt node)
        {
            String name = node.GetId().Text;
        }

        public override void OutAVarAssignstmt(AVarAssignstmt node)
        {
            String name = node.GetId().Text;
            Console.WriteLine("stloc " + name);
        }

        // PRODUCTION 9 -------------------------------------------------------------
        // expr
        public override void OutAOrExpr(AOrExpr node)
        {
            //
            Console.WriteLine("or");
            if (node.GetAndexpr().ToString().Contains("and") ||
                node.GetAndexpr().ToString().Contains("not") ||
                node.GetAndexpr().ToString().Contains("<") ||
                node.GetAndexpr().ToString().Contains(">") ||
                node.GetAndexpr().ToString().Contains("==") ||
                node.GetAndexpr().ToString().Contains("!="))
            {
                Console.WriteLine("brfalse L" + (loopCounter + 1).ToString());
                //Console.WriteLine("L" + loopCounter.ToString() + ":");
                //loopCounter++;
            }
        }

        public override void InAPassExpr(APassExpr node)
        {
            base.InAPassExpr(node);
        }

        public override void OutAPassExpr(APassExpr node)
        {
            if (node.GetAndexpr().ToString().Contains("and") ||
                node.GetAndexpr().ToString().Contains("not") ||
                node.GetAndexpr().ToString().Contains("<") ||
                node.GetAndexpr().ToString().Contains(">") ||
                node.GetAndexpr().ToString().Contains("==") ||
                node.GetAndexpr().ToString().Contains("!="))
            {
                Console.WriteLine("brfalse L" + (loopCounter).ToString());
                //Console.WriteLine("L" + loopCounter.ToString() + ":");
                //loopCounter++;
            }
        }

        // PRODUCTION 10 --------------------------------------------------------------
        // andexpr
        public override void OutAAndAndexpr(AAndAndexpr node)
        {
            Console.WriteLine("and");
        }

        public override void InAPassAndexpr(APassAndexpr node)
        {
            base.InAPassAndexpr(node);
        }

        // PRODUCTION 11 -------------------------------------------------------------
        // notexpr
        public override void OutANotNotexpr(ANotNotexpr node)
        {
            Console.WriteLine("not");
        }

        public override void InAPassNotexpr(APassNotexpr node)
        {
            base.InAPassNotexpr(node);
        }

        // PRODCTION 12 --------------------------------------------------------------
        // booltypes
        public override void InALessBooltypes(ALessBooltypes node)
        {
            base.InALessBooltypes(node);
        }

        public override void InAGrBooltypes(AGrBooltypes node)
        {
            base.InAGrBooltypes(node);
        }

        public override void InALesseqBooltypes(ALesseqBooltypes node)
        {
            base.InALesseqBooltypes(node);
        }

        public override void InAGreqBooltypes(AGreqBooltypes node)
        {
            base.InAGreqBooltypes(node);
        }

        public override void InAEqBooltypes(AEqBooltypes node)
        {
            base.InAEqBooltypes(node);
        }

        public override void InANeqBooltypes(ANeqBooltypes node)
        {
            base.InANeqBooltypes(node);
        }

        public override void InAPassBooltypes(APassBooltypes node)
        {
            base.InAPassBooltypes(node);
        }

        // PRODUCTION 13 -------------------------------------------------------------
        // boollesseq
        public override void OutAExprBoollesseq(AExprBoollesseq node)
        {
            Console.WriteLine("cgt");
            Console.WriteLine("not");
        }

        // PRODUCTION 14 -------------------------------------------------------------
        // boolgreq
        public override void OutAExprBoolgreq(AExprBoolgreq node)
        {
            Console.WriteLine("clt");
            Console.WriteLine("not");
        }

        // PRODUCTION 15 --------------------------------------------------------------
        // boolless
        public override void OutAExprBoolless(AExprBoolless node)
        {
            Console.WriteLine("clt");
        }

        // Production 16 -------------------------------------------------------------
        // boolgr
        public override void OutAExprBoolgr(AExprBoolgr node)
        {
            Console.WriteLine("cgt");
        }

        // PRODUCTION 17 -------------------------------------------------------------
        // booleq
        public override void OutAExprBooleq(AExprBooleq node)
        {
            Console.WriteLine("ceq");
        }

        // PRODUCTION 18 -------------------------------------------------------------
        // boolneq
        public override void OutAExprBoolneq(AExprBoolneq node)
        {
            Console.WriteLine("ceq");
            Console.WriteLine("not");
        }

        // PRODUCTION 19 ---------------------------------------------------------------
        // math expr
        public override void OutAMinusMathexpr(AMinusMathexpr node)
        {
            Console.WriteLine("sub");
        }

        public override void OutAAddMathexpr(AAddMathexpr node)
        {
            Console.WriteLine("add");
        }

        public override void InAPassMathexpr(APassMathexpr node)
        {
            base.InAPassMathexpr(node);
        }

        // PRODUCTION 20 ---------------------------------------------------------------
        // divmulti
        public override void OutADivDivmulti(ADivDivmulti node)
        {
            Console.WriteLine("div");
        }

        public override void OutAMultiDivmulti(AMultiDivmulti node)
        {
            Console.WriteLine("mul");
        }

        public override void InAParenDivmulti(AParenDivmulti node)
        {
            base.InAParenDivmulti(node);
        }

        // PRODUCTION 21 ----------------------------------------------------------------
        // paren
        public override void InAExprParen(AExprParen node)
        {
            base.InAExprParen(node);
        }

        public override void InANegexprParen(ANegexprParen node)
        {
            base.InANegexprParen(node);
        }

        public override void InAPassParen(APassParen node)
        {
            base.InAPassParen(node);
        }

        // PRODUCTION 22 ---------------------------------------------------------------
        // term choose
        public override void InANonegTermchoose(ANonegTermchoose node)
        {
            base.InANonegTermchoose(node);
        }

        public override void InANegTermchoose(ANegTermchoose node)
        {
            base.InANegTermchoose(node);
        }

        // PRODUCTION 23 ---------------------------------------------------------------
        // term
        public override void InAFloatTerm(AFloatTerm node)
        {
            string value = node.GetFloat().Text;
            Console.WriteLine("ldc.r4 " + value);
        }

        public override void InAIntegerTerm(AIntegerTerm node)
        {
            string value = node.GetInt().Text;
            Console.WriteLine("ldc.i4 " + value);
        }

        public override void InAIdentifierTerm(AIdentifierTerm node)
        {
            string id = node.ToString().Trim();
            if (id != "int" && id != "float")
            {
                Console.WriteLine("ldloc " + node.ToString().Trim());
            }
        }

        public override void InAArrayTerm(AArrayTerm node)
        {
            Console.WriteLine("ldloc " + node.GetId().Text);
            //Console.WriteLine("ldc.i4 " + node.GetExpr());
           /* if (node.GetId().Text.StartsWith("i"))
            {
                Console.WriteLine("ldelem.i4");
            }
            else if (node.GetId().Text.StartsWith("f"))
            {
                Console.WriteLine("ldelem.r4");
            } */
        }

        public override void OutAArrayTerm(AArrayTerm node)
        {
            if (node.GetId().Text.StartsWith("i"))
            {
                Console.WriteLine("ldelem.i4");
            }
            else if (node.GetId().Text.StartsWith("f"))
            {
                Console.WriteLine("ldelem.r4");
            }
        }

        // PRODUCTION 24 -----------------------------------------------------------------
        // func
        public override void InAFuncFunc(AFuncFunc node)
        {
            string name = node.GetId().Text.Trim();
            Console.Write(".method static void " + name + "(");
        }

        public override void OutAFuncFunc(AFuncFunc node)
        {

            //Console.WriteLine("");
        }

        public override void InANobodyFunc(ANobodyFunc node)
        {
            base.InANobodyFunc(node);
        }

        public override void InANothingFunc(ANothingFunc node)
        {
            string name = node.GetId().Text.Trim();
            Console.WriteLine(".method static void " + name + "() cil managed {");
        }

        public override void InANoparamFunc(ANoparamFunc node)
        {
            string name = node.GetId().Text.Trim();
            Console.WriteLine(".method static void " + name + "() cil managed {");
        }

        // PRODUCTION 25 ----------------------------------------------------------------
        // formal list
        /* public override void InAOneFormallist(AOneFormallist node)
         {
             base.InAOneFormallist(node);
         } */

        public override void InAOneFormallist(AOneFormallist node)
        {
            //string type = node.GetType().text
            string param = node.GetParam().ToString().TrimEnd();
            string[] parames = param.Split(' ');
            if (param.Contains("float"))
            {
                Console.Write("float32 " + parames[1] );
            }
            else if (param.Contains("int"))
            {
                Console.Write("int32 " + parames[1] );
            }
            else
            {
                Console.Write("string " + parames[1] );
            }

            Console.WriteLine(") cil managed {");
            //argumentCounter++;
        }

        public override void OutAOneFormallist(AOneFormallist node)
        {
            string param = node.GetParam().ToString();
            string[] parames = param.Split(' ');
            //Console.WriteLine(param);
            if (param.Contains("float"))
            {
                Console.WriteLine(".locals init(float32 " + parames[1] + ")");
            }
            else if (param.Contains("int"))
            {
                Console.WriteLine(".locals init(int32 " + parames[1] + ")");
            }
            else
            {
                Console.WriteLine(".locals init(string " + parames[1] + ")");
            }
            Console.WriteLine("ldarg " + argumentCounter.ToString());
            Console.WriteLine("stloc " + parames[1]);
            argumentCounter--;
        }

        public override void OutAManyFormallist(AManyFormallist node)
        {

            string param = node.GetParam().ToString();
            string[] parames = param.Split(' ');
            //Console.WriteLine(param);
            if (param.Contains("float"))
            {
                Console.WriteLine(".locals init(float32 " + parames[1] + ")");
            }
            else if (param.Contains("int"))
            {
                Console.WriteLine(".locals init(int32 " + parames[1] + ")");
            }
            else
            {
                Console.WriteLine(".locals init(string " + parames[1] + ")");
            }
            Console.WriteLine("ldarg " + argumentCounter.ToString());
            Console.WriteLine("stloc " + parames[1]);
            argumentCounter--;
        }

        public override void InAManyFormallist(AManyFormallist node)
        {
            string param = node.GetParam().ToString().TrimEnd();
            string[] parames = param.Split(' ');
            if (param.Contains("float"))
            {
                Console.Write("float32 " + parames[1] + ",");
            }
            else if (param.Contains("int"))
            {
                Console.Write("int32 " + parames[1] + ",");
            }
            else
            {
                Console.Write("string " + parames[1] + ",");
            }
            argumentCounter++;
        }

        // PROUDCTION 26 ----------------------------------------------------------------
        // param
        public override void InAParam(AParam node)
        {
            /*string type = node.GetType().Text;
            type = type.Trim();
            string name = node.GetName().Text;
            name = name.Trim();
            Console.Write(type + " " + name);*/
        }

        public override void OutAParam(AParam node)
        {
            /*string type = node.GetType().Text.Trim();
            string name = node.GetName().Text.Trim();
            Console.Write(type + " " + name); */
        }

        // PRODUCTION 27 ----------------------------------------------------------------
        // proc call
        public override void InAEmptyProccall(AEmptyProccall node)
        {
            string name = node.GetId().Text.Trim();
            Console.WriteLine("call void Program::" + name + "()");
        }

        public override void InATermProccall(ATermProccall node)
        {
            /*string name = node.GetProclist().ToString();
            if (name.Contains("float"))
            {
                Console.WriteLine("ldc.r4" );
            }*/
        }

        public override void OutATermProccall(ATermProccall node)
        {
            string name = node.GetId().Text.Trim();
            if (name == "print_s")
            {
                //Console.WriteLine("ldloc " + node.GetProclist());
                Console.WriteLine("call void [mscorlib]System.Console::WriteLine(string)");
            }
            else if (name == "print_i")
            {
                // Console.WriteLine("ldloc " + node.GetProclist());
                Console.WriteLine("call void [mscorlib]System.Console::WriteLine(int32)");
            }
            else if (name == "print_f")
            {
                // Console.WriteLine("ldloc " + node.GetProclist());
                Console.WriteLine("call void [mscorlib]System.Console::WriteLine(float32)");
            }
            else
            {
                Console.WriteLine("call void Program::" + name + "(int32, float32)");
            }
        }

        // PRODUCTION 28 ----------------------------------------------------------------
        // proc list
        public override void OutAManyProclist(AManyProclist node)
        {
            //base.InAManyProclist(node);
            //Console.Write("" + node.GetProcterm() + ",");
        }

        public override void OutAOneProclist(AOneProclist node)
        {
            //Console.Write(node.GetProcterm() +")");
        }

        // PRODUCTION 29 ----------------------------------------------------------------
        // proc term
        public override void InAMathProcterm(AMathProcterm node)
        {
            string type = node.GetExpr().ToString();
            //string[] name = type.Trim(' ');
        }

        public override void InAStringProcterm(AStringProcterm node)
        {
            string s = node.GetString().Text;
            Console.WriteLine("ldstr " + s);
        }


    } // END CODE GENERATOR
}
