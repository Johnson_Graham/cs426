﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cs426.node;

namespace cs426
{
    class SemanticAnalyzer : cs426.analysis.DepthFirstAdapter
    {
        System.Collections.Generic.Dictionary<string, Definition>
        stringhash = new Dictionary<string, Definition>();
        System.Collections.Generic.Dictionary<cs426.node.Node, Definition>
        nodehash = new Dictionary<node.Node, Definition>();
        System.Collections.Generic.Dictionary<string, Definition>
        statichash = new Dictionary<string, Definition>();

        public override void InAMainfuncProgram(AMainfuncProgram node)
        {
            BasicType inttype = new BasicType();
            inttype.name = "int";
            BasicType floattype = new BasicType();
            floattype.name = "float";
            BasicType stringtype = new BasicType();
            stringtype.name = "string";
            BasicType booltype = new BasicType();
            booltype.name = "bool";
            stringhash.Add(inttype.name, inttype);
            stringhash.Add(floattype.name, floattype);
            stringhash.Add(stringtype.name, stringtype);
            stringhash.Add(booltype.name, booltype);
        }

        public override void InAFuncProgram(AFuncProgram node)
        {
            BasicType inttype = new BasicType();
            inttype.name = "int";
            BasicType floattype = new BasicType();
            floattype.name = "float";
            BasicType stringtype = new BasicType();
            stringtype.name = "string";
            BasicType booltype = new BasicType();
            booltype.name = "bool";
            stringhash.Add(inttype.name, inttype);
            stringhash.Add(floattype.name, floattype);
            stringhash.Add(stringtype.name, stringtype);
            stringhash.Add(booltype.name, booltype);
        }

        public override void OutANoelseIfprod(ANoelseIfprod node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetExpr(), out var);
            Definition booltype;
            stringhash.TryGetValue("bool", out booltype);

            if (var != (booltype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetIf().Line + "]: If expression isn't bool.");
            }
        }

        public override void OutAElseIfprod(AElseIfprod node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetExpr(), out var);
            Definition booltype;
            stringhash.TryGetValue("bool", out booltype);

            if (var != (booltype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetIf().Line + "]: If expression isn't bool.");
            }
        }

        public override void OutAEmptyIfprod(AEmptyIfprod node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetExpr(), out var);
            Definition booltype;
            stringhash.TryGetValue("bool", out booltype);

            if (var != (booltype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetIf().Line + "]: If expression isn't bool.");
            }
        }

        public override void OutAWhileWhileprod(AWhileWhileprod node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetExpr(), out var);
            Definition booltype;
            stringhash.TryGetValue("bool", out booltype);

            if (var != (booltype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetWhile().Line + "]: While expression isn't bool.");
            }
        }

        public override void OutAEmptyWhileprod(AEmptyWhileprod node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetExpr(), out var);
            Definition booltype;
            stringhash.TryGetValue("bool", out booltype);

            if (var != (booltype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetWhile().Line + "]: While expression isn't bool.");
            }
        }

        public override void OutAOrExpr(AOrExpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetAndexpr(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetExpr(), out vartwo);
            Definition booltype;
            stringhash.TryGetValue("bool", out booltype);

            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetOr().Line + "]: Types in expression do not match.");
            }
            else if (var != (booltype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetOr().Line + "]: Types aren't bool.");
            }
            nodehash.Add(node, booltype as TypeDefinition);
        }

        public override void OutAAndAndexpr(AAndAndexpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetAndexpr(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetNotexpr(), out vartwo);
            Definition booltype;
            stringhash.TryGetValue("bool", out booltype);

            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetAnd().Line + "]: Types in expression do not match.");
            }
            else if (var != (booltype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetAnd().Line + "]: Types aren't bool.");
            }
            nodehash.Add(node, booltype as TypeDefinition);
        }

        public override void OutANotNotexpr(ANotNotexpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetBooltypes(), out var);
            Definition booltype;
            stringhash.TryGetValue("bool", out booltype);

            if (var != (booltype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetNot().Line + "]: Type isn't bool.");
            }
            nodehash.Add(node, booltype as TypeDefinition);
        }

        public override void OutANeqBooltypes(ANeqBooltypes node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetBoolneq(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAEqBooltypes(AEqBooltypes node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetBooleq(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAGreqBooltypes(AGreqBooltypes node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetBoolgreq(), out var);
            nodehash.Add(node, var);
        }

        public override void OutALesseqBooltypes(ALesseqBooltypes node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetBoollesseq(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAGrBooltypes(AGrBooltypes node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetBoolgr(), out var);
            nodehash.Add(node, var);
        }

        public override void OutALessBooltypes(ALessBooltypes node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetBoolless(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAPassExpr(APassExpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetAndexpr(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAPassAndexpr(APassAndexpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetNotexpr(), out var);
            nodehash.Add(node, var);
        }


        public override void OutAPassNotexpr(APassNotexpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetBooltypes(), out var);
            nodehash.Add(node, var);
        }


        public override void OutAPassBooltypes(APassBooltypes node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetMathexpr(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAExprBoolneq(AExprBoolneq node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetOne(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetTwo(), out vartwo);
            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetNeq().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetNeq().Line + "]: Types aren't integer or float.");
            }
            Definition typedefn;
            stringhash.TryGetValue("bool", out typedefn);
            nodehash.Add(node, typedefn as TypeDefinition);
        }

        public override void OutAExprBooleq(AExprBooleq node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetOne(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetTwo(), out vartwo);
            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetEq().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetEq().Line + "]: Types aren't integer or float.");
            }
            Definition typedefn;
            stringhash.TryGetValue("bool", out typedefn);
            nodehash.Add(node, typedefn as TypeDefinition);
        }

        public override void OutAExprBoolgr(AExprBoolgr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetOne(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetTwo(), out vartwo);
            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetGrt().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetGrt().Line + "]: Types aren't integer or float.");
            }
            Definition typedefn;
            stringhash.TryGetValue("bool", out typedefn);
            nodehash.Add(node, typedefn as TypeDefinition);
        }

        public override void OutAExprBoolless(AExprBoolless node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetOne(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetTwo(), out vartwo);
            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetLs().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetLs().Line + "]: Types aren't integer or float.");
            }
            Definition typedefn;
            stringhash.TryGetValue("bool", out typedefn);
            nodehash.Add(node, typedefn as TypeDefinition);
        }

        public override void OutAExprBoolgreq(AExprBoolgreq node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetOne(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetTwo(), out vartwo);
            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetGrteq().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetGrteq().Line + "]: Types aren't integer or float.");
            }
            Definition typedefn;
            stringhash.TryGetValue("bool", out typedefn);
            nodehash.Add(node, typedefn as TypeDefinition);
        }

        public override void OutAExprBoollesseq(AExprBoollesseq node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetOne(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetTwo(), out vartwo);
            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetLseq().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetLseq().Line + "]: Types aren't integer or float.");
            }
            Definition typedefn;
            stringhash.TryGetValue("bool", out typedefn);
            nodehash.Add(node, typedefn as TypeDefinition);
        }

        public override void OutAPassMathexpr(APassMathexpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetDivmulti(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAMinusMathexpr(AMinusMathexpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetMathexpr(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetDivmulti(), out vartwo);

            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);

            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetMinus().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetMinus().Line + "]: Types aren't integer or float.");
            }
            nodehash.Add(node, var);
        }

        public override void OutAAddMathexpr(AAddMathexpr node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetMathexpr(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetDivmulti(), out vartwo);

            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetPlus().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetPlus().Line + "]: Types aren't integer or float.");
            }
            nodehash.Add(node, var);
        }

        public override void OutADivDivmulti(ADivDivmulti node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetParen(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetDivmulti(), out vartwo);
            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetDiv().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetDiv().Line + "]: Types aren't integer or float.");
            }
            nodehash.Add(node, var);
        }

        public override void OutAMultiDivmulti(AMultiDivmulti node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetParen(), out var);
            Definition vartwo;
            nodehash.TryGetValue(node.GetDivmulti(), out vartwo);
            Definition inttype;
            stringhash.TryGetValue("int", out inttype);
            Definition floattype;
            stringhash.TryGetValue("float", out floattype);
            if (var != vartwo)
            {
                Console.WriteLine("[Line " + node.GetMult().Line + "]: Types in expression do not match.");
            }
            else if (var != (inttype as TypeDefinition) && var != (floattype as TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetMult().Line + "]: Types aren't integer or float.");
            }
            nodehash.Add(node, var);
        }

        public override void OutAParenDivmulti(AParenDivmulti node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetParen(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAExprParen(AExprParen node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetExpr(), out var);
            nodehash.Add(node, var);
        }

        public override void OutANegexprParen(ANegexprParen node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetExpr(), out var);
            nodehash.Add(node, var);
        }

        public override void OutANonegTermchoose(ANonegTermchoose node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetTerm(), out var);
            nodehash.Add(node, var);
        }

        public override void OutANegTermchoose(ANegTermchoose node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetTerm(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAPassParen(APassParen node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetTermchoose(), out var);
            nodehash.Add(node, var);
        }

        public override void OutAFloatTerm(AFloatTerm node)
        {
            Definition typedefn;
            stringhash.TryGetValue("float", out typedefn);
            nodehash.Add(node, typedefn as TypeDefinition);
        }

        public override void OutAIntegerTerm(AIntegerTerm node)
        {
            Definition typedefn;
            stringhash.TryGetValue("int", out typedefn);
            nodehash.Add(node, typedefn as TypeDefinition);
        }
        /*
        public override void OutAArrayTerm(AArrayTerm node)
        {
            Definition exprdefn;
            nodehash.TryGetValue(node.GetExpr(), out exprdefn);

            Definition typedefn;
            stringhash.TryGetValue("int", out typedefn);

            Definition iddefn;
            if (!stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " + node.GetId().Text + " is not defined.");
            }
            else if (!(iddefn is ArrayDefinition))
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " + node.GetId().Text + " is not an array.");
            }
            else if (typedefn != exprdefn)
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: Array index must be an integer.");
            }
            else if ((iddefn as ArrayDefinition).arraytype != null) //Make sure we have an array.
            {
                nodehash.Add(node, (iddefn as ArrayDefinition).arraytype);
            }
            else
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " + node.GetId().Text + " is not a variable.");
            }
        }

        public override void OutAIdentifierTerm(AIdentifierTerm node)
        {
            Definition iddefn;
            if (!stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " + node.GetId().Text + " is not defined.");
            }
            else if (!(iddefn is TypeDefinition))
            {
                nodehash.Add(node, (iddefn as VariableDefinition).vartype);
            }
            else
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " + node.GetId().Text + " is a type not a variable.");
            }
        }

        public override void OutAIntfloatVardecl(AIntfloatVardecl node)
        {
            string typename = node.GetType().Text;
            string varname = node.GetName().Text;
            Definition typedefn;
            Definition vardefn;
            // lookup the type
            if (!stringhash.TryGetValue(typename, out typedefn))
            {
                Console.WriteLine("[Line " + node.GetType().Line + "]: " +
                    typename + " is not a defined type.");
            }
            // check to make sure what we got back is a type
            else if (!(typedefn is TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " +
                    typename + " is not a type.");
            }
            else
            {
                // add this variable to the hash table
                // note you need to add checks to make sure this 
                // variable name isn't already defined.
                if (!stringhash.TryGetValue(varname, out vardefn))
                {
                    VariableDefinition varnew = new VariableDefinition();
                    varnew.name = varname; // Name of variable
                    varnew.vartype = typedefn as TypeDefinition; //Type of variable
                    stringhash.Add(varnew.name, varnew); //Add variable because it doesn't exist.
                }
                else
                {
                    Console.WriteLine("[Line " + node.GetStop().Line + "]: " + varname + " is already defined.");
                }
            }
        }

        public override void OutAArrayVardecl(AArrayVardecl node)
        {
            string typename = node.GetType().Text;
            string varname = node.GetName().Text;
            Definition length;
            Definition typedefn;
            Definition vardefn;

            // We get the definition of int and compare it to the expr.
            Definition lengthtype;
            stringhash.TryGetValue("int", out lengthtype);

            // lookup the type
            if (!stringhash.TryGetValue(typename, out typedefn))
            {
                Console.WriteLine("[Line " + node.GetType().Line + "]: " +
                    typename + " is not a defined type.");
            }
            // check to make sure what we got back is a type
            else if (!(typedefn is TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " +
                    typename + " is not a type.");
            }
            else if (!nodehash.TryGetValue(node.GetExpr(), out length))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Array size not initialized.");
            }
            else if (lengthtype != length)
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Length of the array must be an integer.");
            }
            else
            {
                // add this variable to the hash table
                // note you need to add checks to make sure this 
                // variable name isn't already defined.
                if (!stringhash.TryGetValue(varname, out vardefn))
                {
                    ArrayDefinition varnew = new ArrayDefinition();
                    varnew.name = varname; // Name of variable
                    varnew.arraytype = typedefn as TypeDefinition; //Type of variable
                    varnew.length = length as BasicType; // Length of array
                    stringhash.Add(varnew.name, varnew); //Add variable because it doesn't exist.
                    statichash.Add(varnew.name, varnew);
                }
                else
                {
                    Console.WriteLine("[Line " + node.GetStop().Line + "]: " + varname + " is already defined.");
                }
            }
        }

        public override void OutAVarAssignstmt(AVarAssignstmt node)
        {
            Definition exprdefn;
            Definition iddefn;
            Definition staticdefn;
            nodehash.TryGetValue(node.GetExpr(), out exprdefn);
            if (!stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Variable is not defined.");
            }
            else if (!(iddefn is VariableDefinition))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " + node.GetId().Text + " is declared but not a variable.");
            }
            else if ((iddefn as VariableDefinition).vartype != exprdefn)
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Expression type is not matching variable type.");
            }
            else if (statichash.TryGetValue(node.GetId().Text, out staticdefn))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Variable is constant and cannot be changed.");
            }
        }

        public override void OutAConstvarAssignstmt(AConstvarAssignstmt node)
        {
            string typename = node.GetType().Text;
            string varname = node.GetName().Text;
            Definition typedefn;
            Definition vardefn;

            Definition exprdefn;
            nodehash.TryGetValue(node.GetExpr(), out exprdefn);
            // lookup the type
            if (!stringhash.TryGetValue(typename, out typedefn))
            {
                Console.WriteLine("[Line " + node.GetType().Line + "]: " +
                    typename + " is not a defined type.");
            }
            // check to make sure what we got back is a type
            else if (!(typedefn is TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " +
                    typename + " is not a type.");
            }
            // Check for type to be the same as the expression type.
            else if (typedefn != (exprdefn as BasicType))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Expression type is not matching constant type.");
            }
            else
            {
                // add this variable to the hash table
                // note you need to add checks to make sure this 
                // variable name isn't already defined.
                if (!stringhash.TryGetValue(varname, out vardefn))
                {
                    VariableDefinition varnew = new VariableDefinition();
                    varnew.name = varname; // Name of variable
                    varnew.vartype = typedefn as TypeDefinition; //Type of variable
                    stringhash.Add(varnew.name, varnew); //Add variable because it doesn't exist.
                    statichash.Add(varnew.name, varnew); //Add variable to a table to state if it's static.
                }
                else
                {
                    Console.WriteLine("[Line " + node.GetStop().Line + "]: " + varname + " is already defined.");
                }
            }
        }

        public override void OutAArrayAssignstmt(AArrayAssignstmt node)
        {
            Definition valuedefn;
            Definition indexdefn;
            Definition iddefn;
            nodehash.TryGetValue(node.GetValue(), out valuedefn);
            nodehash.TryGetValue(node.GetIndex(), out indexdefn);

            Definition indextype;
            stringhash.TryGetValue("int", out indextype);

            if (!stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Variable is not defined.");
            }
            else if (!(iddefn is ArrayDefinition))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " + node.GetId().Text + " is declared but not an array.");
            }
            else if ((iddefn as ArrayDefinition).arraytype != valuedefn)
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Expression type is not matching array type.");
            }
            else if (indextype != indexdefn)
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: Index must be an integer.");
            }
        }

        public override void OutAEmptyProccall(AEmptyProccall node)
        {
            Definition iddefn;
            if (!stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " +
                    node.GetId().Text + " is not defined.");
            }
            else if (!(iddefn is Procedure))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " +
                    node.GetId().Text + " is not a function.");
            }
        }

        public override void OutATermProccall(ATermProccall node)
        {
            Definition iddefn;
            if (!stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " +
                    node.GetId().Text + " is not defined.");
            }
            else if (!(iddefn is Procedure))
            {
                Console.WriteLine("[Line " + node.GetStop().Line + "]: " +
                    node.GetId().Text + " is not a function.");
            }
        }

        public override void OutANothingFunc(ANothingFunc node)
        {
            Definition iddefn;
            if (stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " +
                    node.GetId().Text + " is already defined.");
            }
            else
            {
                Procedure varnew = new Procedure();
                varnew.name = node.GetId().Text; // Name of function
                //Add something for params.
                stringhash.Add(varnew.name, varnew);
            }
        }

        public override void OutANoparamFunc(ANoparamFunc node)
        {
            Definition iddefn;
            if (stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " +
                    node.GetId().Text + " is already defined.");
            }
            else
            {
                Procedure varnew = new Procedure();
                varnew.name = node.GetId().Text; // Name of function
                //Add something for params.
                stringhash.Add(varnew.name, varnew);
            }
        }

        public override void OutANobodyFunc(ANobodyFunc node)
        {
            Definition iddefn;
            if (stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " +
                    node.GetId().Text + " is already defined.");
            }
            else
            {
                Procedure varnew = new Procedure();
                varnew.name = node.GetId().Text; // Name of function
                //Add something for params.
                stringhash.Add(varnew.name, varnew);
            }
        }

        public override void OutAFuncFunc(AFuncFunc node)
        {
            Definition iddefn;
            if (stringhash.TryGetValue(node.GetId().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetId().Line + "]: " +
                    node.GetId().Text + " is already defined.");
            }
            else
            {
                Procedure varnew = new Procedure();
                varnew.name = node.GetId().Text; // Name of function
                //Add something for params.
                stringhash.Add(varnew.name, varnew);
            }
        }

        /*public override void OutAManyFormallist(AManyFormallist node)
        {
            Definition var, var2;
            nodehash.TryGetValue(node.GetParam(), out var);
            nodehash.Add(node, var);
            nodehash.TryGetValue(node.GetFormallist(), out var2);
            nodehash.Add(node, var2);
        }

        public override void OutAOneFormallist(AOneFormallist node)
        {
            Definition var;
            nodehash.TryGetValue(node.GetParam(), out var);
            nodehash.Add(node, var);
        }*/

        public override void OutAParam(AParam node)
        {
            Definition iddefn;
            Definition typedefn;

            if (stringhash.TryGetValue(node.GetName().Text, out iddefn))
            {
                Console.WriteLine("[Line " + node.GetName().Line + "]: " +
                    node.GetName().Text + " is already defined.");
            }
            else if (!stringhash.TryGetValue(node.GetType().Text, out typedefn))
            {
                Console.WriteLine("[Line " + node.GetType().Line + "]: " +
                    node.GetType().Text + " is not a defined type.");
            }
            // check to make sure what we got back is a type
            else if (!(typedefn is TypeDefinition))
            {
                Console.WriteLine("[Line " + node.GetType().Line + "]: " +
                    node.GetType().Text + " is not a type.");
            }
            else
            {
                /*Definition var;
                stringhash.TryGetValue(node., out var);
                Console.WriteLine(var); */
            }
        }
    }
}
